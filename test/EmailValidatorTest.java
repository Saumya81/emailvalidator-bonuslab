import org.junit.*;
 import org.junit.Assert.*;
import org.junit.jupiter.api.Test;
import org.*;

class EmailValidatorTest {

//  ----------------isValidEmailFormat-----------------
	
	@Test
	public final void testIsValidEmailFormat() {
		String email="SaumyaMehta911@gmail.com";
		boolean result=EmailValidator.isValidEmailFormat(email);
		assertTrue("The email provided is valid ",result);
	}

	@Test
	public final void testIsValidEmailFormatIncorrect() {
		String email= "exampleemail.com";
		boolean result=EmailValidator.isValidEmailFormat(email);
		assertFalse("The emai provided is not valid ",result);
	}

	@Test
	public final void testIsValidEmailFormatBoundaryIn() {
		String email="sm@gmail.com";
		boolean result=EmailValidator.isValidEmailFormat(email);
		assertTrue("The email provided is valid ",result);
	}

	@Test
	public final void testIsValidEmailFormatBoundaryOut() {
		String email="saumyamehta911gmail.com";
		boolean result=EmailValidator.isValidEmailFormat(email);
		assertFalse("The emai provided in not valid ",result);
	}
	
	@Test
	public final void testHasThreeAlphaInDomain() {
		String email="checkemail@gmail23.com";
		boolean result=EmailValidator.hasAtleastThreeAlphaInDomain(email);
		assertTrue("The email provided has valid format",result);
	}

	@Test
	public final void testHasThreeAlphaInDomainIncorrect() {
		String email= "";
		boolean result=EmailValidator.hasAtleastThreeAlphaInDomain(email);
		assertFalse("The emai provided has not valid format",result);
	}

	@Test
	public final void testHasThreeAlphaInDomainBoundaryIn() {
		String email="sm@sam.com";
		boolean result=EmailValidator.hasAtleastThreeAlphaInDomain(email);
		assertTrue("The email provided has valid format",result);
	}

	@Test
	public final void testHasThreeAlphaInDomainBoundaryOut() {
		String email="sam@sm.com";
		boolean result=EmailValidator.hasAtleastThreeAlphaInDomain(email);
		assertFalse("The emai provided has not valid format",result);
	}

	
	
		
	//  ----------------hasTwoAlphaInExtension-----------------
	
	@Test
	public final void testHasTwoAlphaInExtension() {
		String email="SaumyaMehta911@gmail.com";
		boolean result=EmailValidator.hasTwoAlphaInExtension(email);
		assertTrue("The email provided has valid format",result);
	}

	@Test
	public final void testHasTwoAlphaInExtensionIncorrect() {
		String email= "";
		boolean result=EmailValidator.hasTwoAlphaInExtension(email);
		assertFalse("The emai provided has not valid format",result);
	}

	@Test
	public final void testHasTwoAlphaInExtensionBoundaryIn() {
		String email="smn@Sam.co";
		boolean result=EmailValidator.hasTwoAlphaInExtension(email);
		assertTrue("The email provided has valid format",result);
	}

	@Test
	public final void testHasTwoAlphaInExtensionBoundaryOut() {
		String email="SaumyaMehta@sam.c";
		boolean result=EmailValidator.hasTwoAlphaInExtension(email);
		assertFalse("The emai provided has not valid format",result);
	}
	
//  ----------------hasOnlyOneAtSign-----------------
	
	@Test
	public final void testHasJustSingleAtSign() {
		String email="SaumyaMehta911@gmail.com";
		boolean result=EmailValidator.hasOnlyOneAtSign(email);
		assertTrue("The email provided is not valid",result);
	}



	@Test
	public final void testHasJustSingleAtSignBoundaryIn() {
		String email="xyz@gmail.com";
		boolean result=EmailValidator.hasOnlyOneAtSign(email);
		assertTrue("The email provided is not valid",result);
	}

	@Test
	public final void testHasJustSingleAtSignBoundaryOut() {
		String email="SaumyaMehta911@@gmail.com";
		boolean result=EmailValidator.hasOnlyOneAtSign(email);
		assertFalse("The emai provided is not valid ",result);
	}
}
