package sheridan;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidator {

	public static boolean hasAtleastThreeAlphaInDomain(String email) {
		int accountNameLength = 0;
		int alphaCount = 0;

		for (int i = 0; i < email.length(); i++) {

			if (email.charAt(i) == '@') {
				break;
			} else {
				accountNameLength++;
			}

		}

		for (int i = accountNameLength + 1; i < email.length(); i++) {

			if (email.charAt(i) == '.') {
				break;
			} else {
				if (Character.isLowerCase(email.charAt(i)) || Character.isDigit(email.charAt(i))) {
					alphaCount++;
				}
			}

		}

		if (alphaCount < 3) {
			return false;
		} else {
			return true;
		}

	}

	public static String regex = "^(.+)@(.+)$";

	public static Pattern pattern = Pattern.compile(regex);


	public static boolean isValidEmailFormat(String email) {
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}

	public static boolean hasOnlyOneAtSign(String email) {
		int count = 0;

		for (int i = 0; i < email.length(); i++) {

			if (count == 2) {
				return false;
			}
			if (email.charAt(i) == '@') {
				count++;
			}
		}

		return true;
	}

public static boolean hasTwoAlphaInExtension(String email) {

		int accountAndDomainLength = 0;
		int alphaCount = 0;

		for (int i = 0; i < email.length(); i++) {

			if (email.charAt(i) == '.') {
				break;
			} else {
				accountAndDomainLength++;
			}

		}

		for (int i = accountAndDomainLength + 1; i < email.length(); i++) {

			if (Character.isDigit(email.charAt(i))) {
				break;
			} else {
				if (Character.isAlphabetic(email.charAt(i))) {
					alphaCount++;
				} else {
					break;
				}
			}

		}

		if (alphaCount < 2) {
			return false;
		} else {
			return true;
		}

	}
}
